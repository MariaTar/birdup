module.exports = {
insertDocument: function(db, callback, query){
	var collection = db.collection(query.collectionName);

	collection.insertMany(query.req).toArray(function(err, docs){
		assert.equal(err, result);
		assert.equal(query.length, result.result.n);
		assert.equal(query.length, result.ops.length);
		console.log("Inserted");
		callback(result);
	})},

findDocument: function(db, callback, query){
  var collection = db.collection(query.collectionName);

  collection.find(query.req).toArray(function(err, docs){
    assert.equal(err, null);
    console.log(docs)
    callback(docs);
  })},

updateDocument: function(db, callback, query) {
  var collection = db.collection(query.collectionName);
 
  collection.updateOne(query.where, { $set: query.what }, function(err, result){
    assert.equal(err, null);
    assert.equal(1, result.result.n);
    console.log("Updated the document");
    callback(result);
  })},

removeDocument: function(db, callback, query) {
  var collection = db.collection(query.collectionName);

  collection.deleteOne(query.what, function(err, result) {
    assert.equal(err, null);
    assert.equal(1, result.result.n);
    console.log("Removed the document");
    callback(result);
  })}
}
