var mongoose = require('mongoose'),
		Schema   = mongoose.Schema;

		var userSchema = new Schema({
			firstName: String,
			lastName: String,
			checkInDate: Number,
			discount: Number,
			phone: Number,
			room: Number,
			login: String,
			password: String
		});

		var User = mongoose.model("User", userSchema);

		module.exports = User;
