var express     = require('express'),
    router      = express.Router(),
    path        = require('path'),
    app         = express(),
    mongoose    = require('mongoose'),
    fs          = require('fs'),
    // MongoClient = require("mongodb").MongoClient,
    assert      = require('assert'),
    user        = require("./app/scripts/dbModels/User"),
    dbtools     = require("./app/scripts/dbmanipulation");

var data = {"data": JSON.parse(fs.readFileSync('app/views/data/data.json', {encoding: 'utf8'}))};
const url = "mongodb://MariaTar:081187mimi@birdup-shard-00-00-4gqkc.mongodb.net:27017,birdup-shard-00-01-4gqkc.mongodb.net:27017,birdup-shard-00-02-4gqkc.mongodb.net:27017/birdup?ssl=true&replicaSet=BirdUp-shard-0&authSource=admin";

mongoose.Promise = global.Promise;

mongoose.connect(url, {useMongoClient: true});
var db = mongoose.connection;
//check connecte to db
db.once('open', function(){
  console.log('connected succecfull');
})

var Schema = mongoose.Schema;

var userSchema = new Schema({
  firstName:{type: String},
  lastName: {type: String},
  age: {type: Number},
  phone: {type: String},
  email: {type: String},
  password: {type: String}
});

var roomSchema = new Schema({
  roomNum: {type: Number, required: true},
  date: {type: Object},
  capacity: {type: Number},
  price: {type: Number}
});

var dateSchema = new Schema({
  date: {type: Number, required: true},
  bookedRoom: {type: Array},
  freeRoom: {type: Array}
});

var bookedSchema = new Schema({
  roomNum: {type: Number},
  arriveDate: {type: Number},
  departDate: {type: Number},
  persons: {type: String},
  booker: {type: String}
});

var UserData = mongoose.model('user', userSchema);
var RoomData = mongoose.model('room', roomSchema);
var DateData = mongoose.model('date', dateSchema);
var BookData = mongoose.model('booking', bookedSchema);


// set up the template engine
app.set('views', path.join(__dirname, 'app/views'));
app.set('view engine', 'pug');


//set up static dir
app.use(express.static("./dist"));
app.use(express.static("./app"));


// GET response for '/', en
// router.get('/', function (req, res, next) {
//   try{
//     res.render('index');  
//   }catch(e){
//     console.log(e);}
//     next(e)}  

// });


router.get('/', function(req, res, next){
  var userObj = UserData.find().
  then(function(user){
    console.log(user);
    });
  res.redirect('/price', userObj);
});

router.get('/gallery', function(req, res){
res.render('_gallery', data)
});

router.get('/about', function(req, res){
res.render('_about', data)
});

router.get('/contacts', function(req, res){
res.render('_contacts', data)
});

router.get('/price', function(req, res){
res.render('_price', data);
});

router.get('/room', function(req, res, next){
  RoomData.find()
    .then(function(doc){
      console.log(doc);
      res.render('index', {rooms: doc});
    })
  });

router.post('/registraton', function(req, res, next){
  var user = {
    firstName: req.body.firstName,
    lastName: req.body.lastName,
    age: req.body.age,
    phone: req.body.phone,
    email: req.bodyemail,
    password: req.body.password
  };
  var newUser = new UserDate(user);
  newUser.save();
});


// router.get('/room', function(req, res, next){
//   var result = [];
//   MongoClient.connect(url, function(err, db){
//     assert.equal(null, err);
//     console.log("connected successfully");
//     var collection = db.collection("rooms").find();
//     collection.forEach(function(doc, err){
//       assert(null, err);
//       result.push(doc);
//     }), function(){
//       db.close();
//       res.render('index', {rooms: result});
//     }
//   });
// });

// router.post('/room', function(req, res, next){
//   var room = {
//     roomNum: req.body.roomNum,
//     arriveDate: req.body.arriveDate,
//     departDate: req.body.departDate,
//     duration: req.body.departDate - req.body.arriveDate,
//     persons: req.body.persons,
//     booker: req.body.booker
//   }
//   MongoClient.connect(url, function(err, db){
//     assert.equal(null, err);
//     db.collection('rooms'.insertOne(item, function(err, res){
//       assert.equal(nul, err);
//       db.close();
//     }))
//   })
// });


router.get("/*", function(req, res){
  res.redirect('/');
});

//Get for ua
router.get('/ua', function (req, res) {
 
 
});

//Get for ru
router.get('/ru', function (req, res) {
 
});
 

app.listen(3000, function () {
    console.log('Listening on http://localhost:3000');
});