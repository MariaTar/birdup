var gulp        = require('gulp'),
    pug         = require('gulp-pug'),
    browserSync = require('browser-sync'),
    //nodemon     = require('gulp-nodemon'),
    cssmin      = require('gulp-cssmin'),
    rename      = require('gulp-rename'),
    imagemin    = require('gulp-imagemin'),
    cache       = require('gulp-cache'),
    data        = require('gulp-data'),
    del         = require('del'),
    fs          =require('fs');


gulp.task('html', function(){
  return gulp.src(['app/views/*.pug', '!app/views/components/*.pug'])
        .pipe(pug({
          locals: {
            "data": JSON.parse(fs.readFileSync('app/views/data/data.json', {encoding: 'utf8'})),
            },
          pretty: true
        }))
        .pipe(gulp.dest('dist'))
        .pipe(browserSync.reload({
          stream: true}));
});

gulp.task('css', ['img'], function(){
  gulp.src('app/css/*.css')
    .pipe(cssmin())
    .pipe(rename({suffix: '.min'}))
    .pipe(gulp.dest('dist'))
    .pipe(browserSync.reload({
          stream: true}));
});

gulp.task('img', function(){
    gulp.src('app/images/*')
      // .pipe(imagemin([
      //   imagemin.gifsicle({interlaced: true}),
      //   imagemin.jpegtran({progressive: true}),
      //   imagemin.optipng({optimizationLevel: 5}),
      //   imagemin.svgo({plugins: [{removeViewBox: true}]})
      // ]))
      .pipe(gulp.dest('dist/images'));

});

// gulp.task('sass', function(){
//   return gulp.src('app/style/*.sass')
//         .pipe(sass())
//         .pipe(gulp.dest('dist'))
//         .pipe(browserSync.reload({
//           stream: true}));
// });

gulp.task('browser', function(){
  browserSync.init(null, {
    server: {
      baseDir: './dist'
    },
    browser: "chrome",
    port: 8000,
    notify: false
  });
});

gulp.task('js', function(){
  return gulp.src('app/scripts/main.js')
  .pipe(gulp.dest('dist'))
  .pipe(browserSync.reload({
          stream: true}));
})

// gulp.task('nodemon', function(cb){
//   var started = false;
//   return nodemon({
//     script: 'app/scripts/main.js',
//     ignore: [
//     'gulpfile.js',
//     'node_modules/'
//     ]
//   }).on('start', function(){
//     //to avoid multiply start
//     if(!started){
//       cb();
//       started = true;
//     };
//   });
// });

gulp.task('clean', function() {
    return del.sync('dist'); // Удаляем папку dist перед сборкой
});

gulp.task('clear', function () {
    return cache.clearAll();
});

gulp.task('watch', ['html', 'css', 'js', 'browser'], function(){
  //gulp.watch('app/views/*.pug', ['html', 'css', 'js']);
  gulp.watch(['app/scrpts/*.js', 'app/view/*.pug'], ['html', 'css', 'js']);
});

gulp.task('default', ['clean', 'watch']);
